//
//  VideosViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/23/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

// Tibold itt kell te dolgozz, erre a screenre jelenits meg dolgokat. Ha a UI-t akarod modositani akkor a Main.storyboardban az also sor harmadik screen az ehhez tartozo UI.


import UIKit
import Alamofire
import SwiftyJSON
//import Cocoa

class VideosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var selectedCategory: String = ""
    
    var videoIds: [String] = [];
    var pictureUrls: [String] = [];
    var titles: [String] = [];
    
    @IBOutlet weak var videoTable: UITableView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoTable.dataSource = self
        videoTable.delegate = self
        videoTable.estimatedRowHeight = 250
        videoTable.rowHeight = UITableViewAutomaticDimension
        self.getData()
        let backbutton = UIButton(type: .custom)
        backbutton.setTitle("Back", for: .normal)
        backbutton.setTitleColor(backbutton.tintColor, for: .normal)
        backbutton.addTarget(self, action: #selector(doneTapped), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        self.navigationItem.title = selectedCategory
    }
    private func buidlData(response: DataResponse<Any>) {
        
    }

    private func getData() {
        if self.titles.count == 0 {
            
            Alamofire.request("https://content.googleapis.com/youtube/v3/search?part=snippet&maxResults=25&q=" +
                self.selectedCategory.lowercased().replacingOccurrences(of: " ", with: "")
                + "&key=AIzaSyDvsOqJWx7uqJL93ONrOg6u3_vd9AFIjfM")
                .validate(contentType: ["application/json"])
                .responseJSON { response in
                    
                    if let data = response.data, let utf8txt = String(data: data, encoding: .utf8) {
                        do {
                            let json = try JSON(data: data)
                            for (_, subJson) in json["items"] {
                              
                                if let videoId = subJson["id"]["videoId"].string {
                                    self.videoIds.insert(videoId, at: 0);
                                }
                                if let title = subJson["snippet"]["title"].string {
                                    self.titles.insert(title, at: 0)
                                }
                                if let image = subJson["snippet"]["thumbnails"]["default"]["url"].string {
                                    self.pictureUrls.insert(image, at: 0)
                                }
                            }
                            self.videoTable.reloadData();
                        } catch {
                            print("Unexpected error: \(error)")
                        }
                    }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func doneTapped() {
        self.dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.videoIds.count)
        return self.videoIds.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "youtubecell", for: indexPath) as! YoutubeListCell

        print("\(indexPath.row)");
        cell.youtubeTitle?.text = self.titles[indexPath.row]
        
        if let ytb_url = NSURL(string: self.pictureUrls[indexPath.row]){
            if let ytb_data = NSData(contentsOf: ytb_url as URL){
                //cell.myImage.contentMode = UIViewContentMode.scaleAspectFit
                cell.youtubeImage?.image = UIImage(data: ytb_data as Data)
              
            }
        }
  
        return (cell)
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let url: String = "https://www.youtube.com/watch?v=" + videoIds[indexPath.row];
        if let url = URL(string: url) {
            UIApplication.shared.openURL(url)
        }
    }
    
    
}
class YoutubeListCell : UITableViewCell {

    @IBOutlet weak var youtubeImage: UIImageView!
    @IBOutlet weak var youtubeTitle: UILabel!
}
