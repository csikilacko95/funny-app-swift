//
//  VideosViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/21/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

// Tibold itt is dolgozz, ha nem kellenek a kategoriak akkor vedd ki, ha nem kell ez a screen akkor vedd ki a story boardrol is.

import UIKit

class VideosTableViewController: UITableViewController {
    let categories:[String] = ["Russian videos", "Dog  videos", "Cat videos", "Crazy driver"]
    let categoryImages:[String] = ["funny-russia.png", "funny-dog.jpg", "funny-cat.png", "crazy-driver.jpg"]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count
    }
    //
    //
    //    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        var cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
    //
    //        if cell == nil {
    //            cell = UITableViewCell(style: .default, reuseIdentifier: "cellIdentifier")
    //        }
    //
    //        print("\(#function) --- section = \(indexPath.section), row = \(indexPath.row)")
    //
    //        cell!.textLabel?.text = categories[indexPath.row]
    //        cell!.imageView?.image = UIImage(named: categoryImages[indexPath.row])
    //        return cell!
    //    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryListCell")
            as! CategoryListCell
        
        cell.categoryText?.text = categories[indexPath.row]
        cell.categoryImage?.image = UIImage(named: categoryImages[indexPath.row])
        
        return cell
    }
    //
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(categories[indexPath.row])
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "videosViewController") as! VideosViewController
        newViewController.selectedCategory = categories[indexPath.row] // pass data to the controller
        let navigationController = UINavigationController(rootViewController: newViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//struct VCategory {
//    var id: Int
//    var catName: String
//    var image: String;
//}
//
//class VCategoryListCell : UITableViewCell {
//
//
//}

class CategoryListCell : UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryText: UILabel!
}
