//
//  JokesViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/23/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

import UIKit
import Alamofire

class JokesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    var selectedCategory: String = ""
//    let categories:[String] = ["Chuck Norris", "Geek Jokes", "All in all"]
    var chuckNorrisJokes = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        print(selectedCategory)
        if chuckNorrisJokes.count == 0 {
           
            switch self.selectedCategory {
                case "Chuck Norris":
                    Alamofire.request("http://api.icndb.com/jokes/random/20").responseJSON { response in
                        if let dict = response.result.value as? Dictionary<String, AnyObject>{
                            if let jokes = dict["value"] {
                                self.chuckNorrisJokes = jokes as! [AnyObject]
                                self.tableView.reloadData();
                            }
                        }
                    }
                    case "Geek Jokes":
                        
                        //Geek jokes
                        Alamofire.request("http://api.icndb.com/jokes/random/20").responseJSON { response in
                            if let dict = response.result.value as? Dictionary<String, AnyObject>{
                                if let jokes = dict["value"] {
                                    self.chuckNorrisJokes = jokes as! [AnyObject]
                                    self.tableView.reloadData();
                                }
                            }
                        }
                default:
                    print("DEFUALT");
            }
        }
        
        let backbutton = UIButton(type: .custom)
        backbutton.setTitle("Back", for: .normal)
        backbutton.setTitleColor(backbutton.tintColor, for: .normal)
        backbutton.addTarget(self, action: #selector(doneTapped), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        self.navigationItem.title = selectedCategory
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func doneTapped() {
        self.dismiss(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chuckNorrisJokes.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! JokesTableViewCell
        print(chuckNorrisJokes[indexPath.row]["joke"])
        cell.label.text = chuckNorrisJokes[indexPath.row]["joke"] as? String
        //        if selectedCategory != "General" {
        //            if let socc_url = NSURL(string: soccer_memes[indexPath.row]){
        //                if let socc_data = NSData(contentsOf: socc_url as URL){
        //                    //cell.myImage.contentMode = UIViewContentMode.scaleAspectFit
        //                    cell.myImage.image = UIImage(data: socc_data as Data)
        //                    cell.myLabel.text = ""
        //                }
        //            }
        //        }
        //        else {
        //            if let myUrl = self.memes[indexPath.row]["url"] as? String {
        //                if let url = NSURL(string: myUrl){
        //                    if let data = NSData(contentsOf: url as URL){
        //                        //cell.myImage.contentMode = UIViewContentMode.scaleAspectFit
        //                        cell.myImage.image = UIImage(data: data as Data)
        //                    }
        //                }
        //                if let title = self.memes[indexPath.row]["name"] as? String {
        //                    cell.myLabel.text = title
        //                }
        //            }
        //        }
        
        return (cell)
    }

}
