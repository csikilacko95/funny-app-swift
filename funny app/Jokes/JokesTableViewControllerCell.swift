//
//  JokesTableViewControllerCell.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/27/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

import UIKit

class JokesTableViewControllerCell: UITableViewCell {

    @IBOutlet weak var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
