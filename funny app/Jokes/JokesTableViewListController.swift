//
//  JokesTableViewListControllerTableViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/27/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

import UIKit
import Alamofire

class JokesTableViewListController: UITableViewController {
    var selectedCategory: String = ""
    //    let categories:[String] = ["Chuck Norris", "Geek Jokes", "All in all"]
    var chuckNorrisJokes = [AnyObject]()
    var geekJokes:[String] = [
    "Chuck Norris can spawn threads that complete before they are started.",
    "A movie scene depicting Chuck Norris losing a fight with Bruce Lee was the product of history's most expensive visual effect. When adjusted for inflation, the effect cost more than the Gross National Product of Paraguay.",
    "The best-laid plans of mice and men often go awry. Even the worst-laid plans of Chuck Norris come off without a hitch.",
    "E-mail returned to sender... insufficient voltage.",
    "Chuck Norris doesn't use a computer because a computer does everything slower than Chuck Norris.",
    "MacGyver can build an airplane out of gum and paper clips. Chuck Norris can kill him and take it.",
    "Erik Naggum: Microsoft is not the answer. Microsoft is the question. NO is the answer.",
    "If you ask Chuck Norris what time it is, he always answers &quot;Two seconds till&quot;. After you ask &quot;Two seconds to what?&quot;, he roundhouse kicks you in the face.",
    "Chuck Norris? roundhouse kick is so powerful, it can be seen from outer space by the naked eye.",
    "Love does not hurt. Chuck Norris does.",
    "Chuck Norris's OSI network model has only one layer - Physical.",
    "July 4th is Independence day. And the day Chuck Norris was born. Coincidence? I think not.",
    "Chuck Norris has to register every part of his body as a separate lethal weapon. His spleen is considered a concealed weapon in over 50 states.",
    "Contrary to popular belief, the Titanic didn't hit an iceberg. The ship was off course and ran into Chuck Norris while he was doing the backstroke across the Atlantic.",
    "In the X-Men movies, none of the X-Men super-powers are done with special effects. Chuck Norris is the stuntman for every character.",
    "Those aren't credits that roll after Walker Texas Ranger. It is actually a list of fatalities that occurred during the making of the episode.",
    "Chuck Norris can access private methods.",
    "Chuck Norris doesn't use web standards as the web will conform to him.",
    "If at first you don't succeed, you're not Chuck Norris."
    ]
    var allJokes = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        print(selectedCategory)
        print(allJokes)
        
        if (self.selectedCategory != "All in all" && self.chuckNorrisJokes.count == 0) || (self.selectedCategory == "All in all" && self.allJokes.count == 0) {
            
            switch self.selectedCategory {
                case "Chuck Norris":
                    Alamofire.request("http://api.icndb.com/jokes/random/20").responseJSON { response in
                        if let dict = response.result.value as? Dictionary<String, AnyObject>{
                            if let jokes = dict["value"] {
                                self.chuckNorrisJokes = jokes as! [AnyObject]
                                self.tableView.reloadData()
                            }
                        }
                    }
                case "Geek Jokes":
                    
                    //Geek jokes
                    Alamofire.request("http://api.icndb.com/jokes/random/20").responseJSON { response in
                        if let dict = response.result.value as? Dictionary<String, AnyObject>{
                            if let jokes = dict["value"] {
                                self.chuckNorrisJokes = jokes as! [AnyObject]
                                self.tableView.reloadData()
                            }
                        }
                    }
                default:
                    let headers: HTTPHeaders = [
                        "Accept": "application/json"
                    ]
                    
                    Alamofire.request("https://icanhazdadjoke.com/search", headers: headers).responseJSON { response in
                        if let dict = response.result.value as? Dictionary<String, AnyObject>{
                            if let jokes = dict["results"] {
                                print(jokes)
                                self.allJokes = jokes as! [AnyObject]
                                self.tableView.reloadData()
                            }
                        }
                    }
            }
        }
        
        let backbutton = UIButton(type: .custom)
        backbutton.setTitle("Back", for: .normal)
        backbutton.setTitleColor(backbutton.tintColor, for: .normal)
        backbutton.addTarget(self, action: #selector(doneTapped), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        self.navigationItem.title = selectedCategory
    }
    
    @objc func doneTapped() {
        self.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.selectedCategory == "Chuck Norris") {
            return self.chuckNorrisJokes.count
        } else if (self.selectedCategory == "Geek Jokes") {
            return self.geekJokes.count
        } else {
            return self.allJokes.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! JokesTableViewCell
        
        print("\(#function) --- section = \(indexPath.section), row = \(indexPath.row)")
        if(selectedCategory == "Chuck Norris") {
            cell.label?.text = self.chuckNorrisJokes[indexPath.row]["joke"] as? String
        } else if (self.selectedCategory == "Geek Jokes") {
            cell.label?.text = self.geekJokes[indexPath.row]
        } else {
            cell.label?.text = self.allJokes[indexPath.row]["joke"] as? String
        }
        
        return cell
    }
 

}
