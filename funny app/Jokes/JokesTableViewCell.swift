//
//  JokesTableViewCell.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/27/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

import UIKit

class JokesTableViewCell: UITableViewCell {

    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var cellView: UIView! {
        didSet {
            self.cellView.backgroundColor = UIColor.white
            contentView.backgroundColor = UIColor(red: 240/255.0, green: 240/255.0, blue: 240/255.0, alpha: 1.0)
            self.cellView.layer.cornerRadius = 3.0
            cellView.layer.masksToBounds = false
            cellView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
            cellView.layer.shadowOffset = CGSize(width: 0, height: 0)
            cellView.layer.shadowOpacity = 0.8
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
