
//
//  ImagesViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/23/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//

import UIKit

class ImagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var selectedCategory: String = ""
    var memes = [AnyObject]()
    var soccer_memes = [String]()
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (selectedCategory != "General")
        {
            return (soccer_memes.count)
        }
        else
        {
            return (memes.count)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImagesTableViewTableViewCell

        if selectedCategory != "General" {
            if let socc_url = NSURL(string: soccer_memes[indexPath.row]){
                if let socc_data = NSData(contentsOf: socc_url as URL){
                    //cell.myImage.contentMode = UIViewContentMode.scaleAspectFit
                    cell.myImage.image = UIImage(data: socc_data as Data)
                    cell.myLabel.text = ""
                }
            }
        }
        else {
            if let myUrl = self.memes[indexPath.row]["url"] as? String {
                if let url = NSURL(string: myUrl){
                    if let data = NSData(contentsOf: url as URL){
                        //cell.myImage.contentMode = UIViewContentMode.scaleAspectFit
                        cell.myImage.image = UIImage(data: data as Data)
                    }
                }
                if let title = self.memes[indexPath.row]["name"] as? String {
                    cell.myLabel.text = title
                }
            }
        }
        
        return (cell)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backbutton = UIButton(type: .custom)
        backbutton.setTitle("Back", for: .normal)
        backbutton.setTitleColor(backbutton.tintColor, for: .normal)
        backbutton.addTarget(self, action: #selector(doneTapped), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
        self.navigationItem.title = selectedCategory
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func doneTapped() {
        self.dismiss(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
