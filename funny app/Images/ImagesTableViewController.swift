
//
//  SecondViewController.swift
//  funny app
//
//  Created by Csiki Laszlo on 1/21/19.
//  Copyright © 2019 Csiki Laszlo. All rights reserved.
//
//

import UIKit
import Alamofire

class ImagesTableViewController: UITableViewController {
    let categories:[String] = ["General", "Soccer Memes"]
    var memes = [AnyObject]()
    var soccer_memes:[String] = ["https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/41448743_2287813191277583_184885620399669248_n.jpg?_nc_cat=105&_nc_ht=scontent-otp1-1.xx&oh=d204293d5347d99bda3b7e35ab300347&oe=5CC74ABC", "https://img.memecdn.com/The-Only-Way-Real-Madrid-Would-Be-Able-To-Beat-Barcelona_o_110963.jpg" ,"https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/49542280_2448883235170577_6789411798173876224_n.jpg?_nc_cat=110&_nc_ht=scontent-otp1-1.xx&oh=2b392413269e9df796065d4581c4e44f&oe=5CBD6036", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/39864436_2257951137597122_6592079727622619136_n.jpg?_nc_cat=111&_nc_ht=scontent-otp1-1.xx&oh=a38fb0ee1d9b05b3db2fc89d4fe2edd2&oe=5CB86BC3" ,"https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/48987121_2439703102755257_1497634098020089856_n.jpg?_nc_cat=107&_nc_ht=scontent-otp1-1.xx&oh=daceade77bfbdf74fb3ed9ae83a1be2d&oe=5CBCB5A7", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/48277510_2424529327605968_8620749427570638848_n.jpg?_nc_cat=101&_nc_ht=scontent-otp1-1.xx&oh=033a98d3376f01292a791267f65e082c&oe=5CC881A3", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/48404165_2423738807685020_552880308106559488_n.jpg?_nc_cat=111&_nc_ht=scontent-otp1-1.xx&oh=84575b1a6c0206873b94aac074e5b149&oe=5CF84F57", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/46703926_2396590253733209_2568973875660455936_n.jpg?_nc_cat=102&_nc_ht=scontent-otp1-1.xx&oh=3020e46d2694be3469aee88289bd1979&oe=5CC86ACE", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/46480013_2389886281070273_2269529971259080704_n.jpg?_nc_cat=101&_nc_ht=scontent-otp1-1.xx&oh=d62674354dc07c06a7c04aaa3e643c17&oe=5CB64F64", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/43828489_2335070359885199_5925054109504765952_n.jpg?_nc_cat=101&_nc_ht=scontent-otp1-1.xx&oh=429f822a99230eef299e9ada8d85d4c5&oe=5CC12D3A", "https://scontent-otp1-1.xx.fbcdn.net/v/t1.0-9/42933039_2315011355224433_3533820811074863104_n.jpg?_nc_cat=111&_nc_ht=scontent-otp1-1.xx&oh=be1279e1965052a4b2a36f4ed7535664&oe=5CF34133"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        
        
    
        Alamofire.request("https://api.imgflip.com/get_memes").responseJSON { response in
            if let dict = response.result.value as? Dictionary<String, AnyObject>{
                print(dict)
                if let innerDict = dict["data"] {
                    if let memes = innerDict["memes"] {
                        //let myArray = memes as! [AnyObject]
                        self.memes = memes as! [AnyObject]
                    }
                }
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return categories.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //var cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ImagesTableViewControllerTableViewCell
        cell.myLabel.text = self.categories[indexPath.row]
        
        return (cell)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "imagesViewController") as! ImagesViewController
        newViewController.selectedCategory = categories[indexPath.row]  // pass data to the controller
        
        
        newViewController.memes = self.memes
        newViewController.soccer_memes = self.soccer_memes
        
        let navigationController = UINavigationController(rootViewController: newViewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
}

